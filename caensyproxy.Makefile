
where_am_I := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
include $(E3_REQUIRE_TOOLS)/driver.makefile


APPSRC := src
APPDB := db

USR_INCLUDES += -I$(where_am_I)$(APPSRC)

TEMPLATES += $(wildcard $(APPDB)/*.db)
TEMPLATES += $(wildcard $(APPDB)/*.template)
SUBS += $(wildcard $(APPDB)/*.substitutions)

HEADERS += $(DBDINC_HDRS)

SOURCES += $(wildcard $(APPSRC)/*.c)

DBDS += $(APPSRC)/nblmpower_asub.dbd

SCRIPTS += $(wildcard ../iocsh/*.iocsh)

USR_DBFLAGS += -I . -I ..
USR_DBFLAGS += -I $(EPICS_BASE)/db
USR_DBFLAGS += -I $(APPDB)

.PHONY: 
.PHONY: vlibs
vlibs:

