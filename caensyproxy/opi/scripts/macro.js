importPackage(Packages.org.csstudio.opibuilder.scriptUtil);

var macroInput = DataUtil.createMacrosInput(true);
var opiPath = widget.getPropertyValue("opi_file");

// get current macro
var HV_REGION = PVUtil.getString(pvs[2]); //PV : $(HV_REGION)
macroInput.put("HV_REGION", HV_REGION);

// group selection
try {
    var groupName = PVUtil.getString(pvs[0]); //PV : NBLM:GUI:GROUP_SELECTION
}
catch(err) {
    var groupName = "";
    ConsoleUtil.writeInfo(err);
}
macroInput.put("PREFIX", groupName);

// nblm selection
try {
    var nBLMNb = PVUtil.getDouble(pvs[1]) + 1; //PV : NBLM:GUI:NBLM_NUMBER_SELECTION (+1 because strating from 1 and not 0)
}
catch(err) {
    var nBLMNb = "";
    ConsoleUtil.writeInfo(err);
}
macroInput.put("NBLM_NB", nBLMNb);

// put new macro
widget.setPropertyValue("macros", macroInput);
// need to close and open the OPI in order new maccro are taken in effect
widget.setPropertyValue("opi_file", "");
widget.setPropertyValue("opi_file", opiPath);



