importPackage(Packages.org.csstudio.opibuilder.scriptUtil);
 
function pad(num, size) {
    var s = "000000000" + num;
    return s.substr(s.length-size);
}

var HV_BOARD    = PVUtil.getDouble(pvArray[0]); // get HV board number, ex: "02"
var HV_CHANNEL  = PVUtil.getDouble(pvArray[1]); // get HV channel number

// double to int to string
HV_BOARD    =  parseInt(HV_BOARD, 10).toString()
HV_CHANNEL  =  parseInt(HV_CHANNEL, 10).toString()

// get opi path
var opiPath = widget.getPropertyValue("opi_file");

// double to formated string
HV_BOARD    = pad(HV_BOARD,     2)  // ex: "02"
HV_CHANNEL  = pad(HV_CHANNEL,   3)  // ex: "001"
// ConsoleUtil.writeInfo("HV_BOARD: "      + HV_BOARD);
// ConsoleUtil.writeInfo("HV_CHANNEL: "    + HV_CHANNEL);

// create a new macro structure and insert
// the nblm macro with the value of the PV
var maccro = DataUtil.createMacrosInput(true);
maccro.put("HV_BOARD",      HV_BOARD);
maccro.put("HV_CHANNEL",    HV_CHANNEL);
widget.setPropertyValue("macros", maccro);

// reload the embedded OPI
widget.setPropertyValue("opi_file", "");
widget.setPropertyValue("opi_file", opiPath);
